Clone project
==
```
git clone https://gitlab.com/noureddine123/goulmima-design-patterns.git
```

Install dependencies
==
From the root directory :
```
composer install
```

Tests
==
From the root directory :

``` 
./vendor/bin/phpunit tests
```
