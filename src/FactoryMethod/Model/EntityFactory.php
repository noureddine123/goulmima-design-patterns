<?php

namespace App\FactoryMethod\Model;

class EntityFactory implements ModelFactory
{
    public function createModel(): ModelInterface
    {
        return new Entity();
    }
}
