<?php

namespace App\FactoryMethod\Model;

/**
 * All data model factories should implements this interface.
 * This approach help you to create dynamically model services.
 *
 * Interface ModelFactory
 * @package App\FactoryMethod\Model
 */
interface ModelFactory
{
    public function createModel(): ModelInterface;
}
