<?php

namespace App\FactoryMethod\Model;

class Document implements ModelInterface
{
    public function identify()
    {
        return "Document";
    }
}
