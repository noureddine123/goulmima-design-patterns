<?php

namespace App\FactoryMethod\Model;

class DocumentFactory implements ModelFactory
{
    public function createModel(): ModelInterface
    {
        return new Document();
    }
}
