<?php

namespace App\FactoryMethod\Model;

class APIFactory implements ModelFactory
{
    public function createModel(): ModelInterface
    {
        return new API();
    }
}
