<?php

namespace App\FactoryMethod\Model;

class Entity implements ModelInterface
{
    public function identify()
    {
        return "Entity";
    }
}
