<?php

namespace App\FactoryMethod\Model;

class API implements ModelInterface
{
    public function identify()
    {
        return "API";
    }
}
