<?php

namespace App\FactoryMethod\Model;

class ModelManager
{
    private $modelFactory;

    public function __construct(ModelFactory $modelFactory)
    {
        $this->modelFactory = $modelFactory;
    }

    public function identifyModel()
    {
        // instantiate one of data model based on ModelManager config.
        $model = $this->modelFactory->createModel();

        // use the matched data model service.
        return $model->identify();
    }
}
