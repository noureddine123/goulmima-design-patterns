<?php

namespace App\FactoryMethod\Model;

/**
 * All data model services should implements this interface.
 *
 * Interface ModelInterface
 * @package App\FactoryMethod\Model
 */
interface ModelInterface
{
    public function identify();
}
