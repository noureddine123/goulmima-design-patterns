Illustration of `FactoryMethod` design pattern by concrete example.


Purpose
==
Construct data model service based on a manager configuration.
