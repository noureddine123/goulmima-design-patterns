<?php

namespace App\Tests\FactoryMethod;

use App\FactoryMethod\Model\APIFactory;
use App\FactoryMethod\Model\DocumentFactory;
use App\FactoryMethod\Model\EntityFactory;
use App\FactoryMethod\Model\ModelManager;
use PHPUnit\Framework\TestCase;

class FactoryMethodTest extends TestCase
{
    public function testCreationOfExactModelInstance()
    {
        $modelFactory = new EntityFactory();
        $modelManager = new ModelManager($modelFactory);
        $modelName = $modelManager->identifyModel();
        $this->assertEquals('Entity', $modelName);

        $modelFactory = new APIFactory();
        $modelManager = new ModelManager($modelFactory);
        $modelName = $modelManager->identifyModel();
        $this->assertEquals('API', $modelName);

        $modelFactory = new DocumentFactory();
        $modelManager = new ModelManager($modelFactory);
        $modelName = $modelManager->identifyModel();
        $this->assertEquals('Document', $modelName);
    }
}
